using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using WebApi.Models;

namespace WebApi.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class LoginController : ControllerBase
	{
		private IConfiguration _config;

        public LoginController(IConfiguration config) 
		{
			_config = config;
		}

		[AllowAnonymous]
		[HttpPost]
		public IActionResult Login([FromBody] User userLogin)
		{
			var user = Authenticate(userLogin);

			if (user != null)
			{
				var token = Generate(user);
				return Ok(token);
			}

			return NotFound("User not found");
		}


		private string Generate(User user)
		{
			var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
			var credntials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.NameIdentifier, user.username),
			};

			var token = new JwtSecurityToken(_config["Jwt:Issuer"],
				_config["Jwt:Audience"],
				claims,
				expires: DateTime.Now.AddMinutes(15),
				signingCredentials: credntials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		private User Authenticate(User user)
		{
			var currentUser = UsersConstant.users.FirstOrDefault(x => x.username.ToLower() == user.username.ToLower() && x.password == user.password);
			
			if (currentUser != null)
			{
				return currentUser;
			}

			return null;
		}
	}
}