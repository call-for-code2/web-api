using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;

namespace WebApi.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class ItemsController : ControllerBase
	{
		private readonly WebApiDbContext _context;

		public ItemsController(WebApiDbContext context)
		{
			_context = context;
		}
		
		[HttpGet]
		[Authorize]
		public async Task<IEnumerable<Item>> GetAll()
		{
			return await _context.Items.ToListAsync();
		}
	}
}