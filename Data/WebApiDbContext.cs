using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Data
{
	public class WebApiDbContext : DbContext
	{
		public WebApiDbContext(DbContextOptions<WebApiDbContext> options) : base(options)
		{
		}

		public DbSet<User> Users { get; set; }
		public DbSet<Item> Items { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().HasData(
				new User { Id = 1, username = "admin", password = "admin" },
				new User { Id = 2, username = "alizoubair", password = "alizoubair" }
			);
			modelBuilder.Entity<Item>().HasData(
				new Item { Id = 1, name = "Book"},
				new Item { Id = 2, name = "Laptop"}
			);
		}
	}
}