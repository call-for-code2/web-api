using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
	public class Item
	{
		public int Id { get; set; }
		[Required]
		public string name { get; set; }
	}
}