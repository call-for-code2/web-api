using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
	public class User
	{
		public int Id { get; set; }
		[Required]
		public string username { get; set; }
		[Required]
		public string password { get; set; }
	}
}